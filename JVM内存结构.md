# 学习JVM的一些相关笔记

## JVM内存结构

jvm内存大致分为五块区：堆，虚拟机栈，本地方法栈，程序计数器，方法区。

属于线程共享：堆，方法区。

属于线程隔离：虚拟机栈，本地方法栈，程序计数器。

如图所示

![image-20210302151738105](https://gitee.com/langren19/jvm/raw/master/image/image-20210302151738105.png)



五块内存分别是做什么的

堆：堆是jvm内存中最大的一块内存空间，绝大部分对象都是存储在堆内存里面的，堆内存又做了细分

![image-20210302151830567](https://gitee.com/langren19/jvm/raw/master/image/image-20210302151830567.png)

虚拟机栈：虚拟机栈是线程独享的，当创建一个线程后就会创建一个虚拟机栈。

![image-20210302151919328](https://gitee.com/langren19/jvm/raw/master/image/image-20210302151919328.png)

本地方法栈：都是由C语言去实现的。

​    

程序计数器：用来记录各个线程之间执行字节码的地址，像分支，循环，跳转，异常，恢复等等操作都需要依                                    赖程序计数器。

方法区：方法也是多个线程共享的，方法区主要包过四部分（如图所示），方法区主要作用是用来存放虚拟机加载的类相关信息。从这张图可以看出方法区存在多个常量池。

![image-20210302152120353](https://gitee.com/langren19/jvm/raw/master/image/image-20210302152120353.png)



 

![image-20210302152213455](https://gitee.com/langren19/jvm/raw/master/image/image-20210302152213455.png)



![image-20210302152242480](https://gitee.com/langren19/jvm/raw/master/image/image-20210302152242480.png)

![image-20210302152304599](https://gitee.com/langren19/jvm/raw/master/image/image-20210302152304599.png)

## 类的加载过程

![image-20210302154633323](https://gitee.com/langren19/jvm/raw/master/image/image-20210302154633323.png)





jvm 是怎么加载一个类的呢？当一个类被创建时或者被引用时虚拟机发现之前没有加载过这个类，就会通过类加载器八class文件加载到内存在加载的过程中主要做的三件事。加载完成之后会进入链接的步骤。

![image-20210302154712066](https://gitee.com/langren19/jvm/raw/master/image/image-20210302154712066.png)

链接这个步骤又可以细分为 验证，准备，解析。其中验证      jvm启动参数   -Xverify：none  关闭验证。



![image-20210302154743306](https://gitee.com/langren19/jvm/raw/master/image/image-20210302154743306.png)



![image-20210302154830873](https://gitee.com/langren19/jvm/raw/master/image/image-20210302154830873.png)



![image-20210302154859048](https://gitee.com/langren19/jvm/raw/master/image/image-20210302154859048.png)



![image-20210302154934465](https://gitee.com/langren19/jvm/raw/master/image/image-20210302154934465.png)

准备环节的作用



![image-20210302155113520](https://gitee.com/langren19/jvm/raw/master/image/image-20210302155113520.png)

解析的作用



![image-20210302155211106](https://gitee.com/langren19/jvm/raw/master/image/image-20210302155211106.png)

初始化阶段

![image-20210302155240832](https://gitee.com/langren19/jvm/raw/master/image/image-20210302155240832.png)



![image-20210302155335282](https://gitee.com/langren19/jvm/raw/master/image/image-20210302155335282.png)

## 编译器优化机制

先来探讨字节码是如何运行的？

分为两种

解释执行：由解释器一行一行翻译执行。

编译执行：把字节码编译成机器码，直接执行机器码 

对比一下解释执行跟编译执行

![image-20210302155657793](https://gitee.com/langren19/jvm/raw/master/image/image-20210302155657793.png)

怎么来查看自己的Java是解释执行的还是编译执行的呢

![image-20210309174100725](https://gitee.com/langren19/jvm/raw/master/image/image-20210309174100725.png)

  mixed mode 代表混合执行，表示部分代码解释执行，部分代码编译执行。

 

![image-20210302160327745](https://gitee.com/langren19/jvm/raw/master/image/image-20210302160327745.png)

设置解释执行 interpreted mode

![image-20210309174226660](https://gitee.com/langren19/jvm/raw/master/image/image-20210309174226660.png)





![image-20210302160417971](https://gitee.com/langren19/jvm/raw/master/image/image-20210302160417971.png)



Hostpot的即时编译器 分为C1 跟C2



![image-20210302160527234](https://gitee.com/langren19/jvm/raw/master/image/image-20210302160527234.png)



![image-20210302160613652](https://gitee.com/langren19/jvm/raw/master/image/image-20210302160613652.png)

各层次的优化 

从jkd7开始正式引入了分层编译，可以细分为五种编译级别。（Profiling jvm 性能监控）

![image-20210302160724329](https://gitee.com/langren19/jvm/raw/master/image/image-20210302160724329.png)

![image-20210302160823976](https://gitee.com/langren19/jvm/raw/master/image/image-20210302160823976.png)

![image-20210302160935784](https://gitee.com/langren19/jvm/raw/master/image/image-20210302160935784.png)

  编译执行主要是针对热点代码的，怎么样找到热点代码呢？

![image-20210302161047194](https://gitee.com/langren19/jvm/raw/master/image/image-20210302161047194.png)

![image-20210302161134832](https://gitee.com/langren19/jvm/raw/master/image/image-20210302161134832.png)

![image-20210302161150715](https://gitee.com/langren19/jvm/raw/master/image/image-20210302161150715.png)

![image-20210302161244073](https://gitee.com/langren19/jvm/raw/master/image/image-20210302161244073.png)

方法调用计数器流程

![image-20210302161311533](https://gitee.com/langren19/jvm/raw/master/image/image-20210302161311533.png)

![image-20210309175731086](https://gitee.com/langren19/jvm/raw/master/image/image-20210309175731086.png)

回边计数器流程

![image-20210302161518778](https://gitee.com/langren19/jvm/raw/master/image/image-20210302161518778.png)

jvm相关参数

![image-20210309180006695](https://gitee.com/langren19/jvm/raw/master/image/image-20210309180006695.png)